package pl.edu.pwsztar.domain.converter;

public interface MovieConverter<T,F> {
    T convert(F from);
}
